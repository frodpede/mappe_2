package no.frodpede;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {

    private Patient patient;

    @BeforeEach
    public void init() {
        patient = new Patient("Name1", "Name2", "Doctor","11223344551");
    }

    @Test
    @DisplayName("Patient constructor throws exception if data is invalid")
    public void createWrongPatientThrows() {
        assertThrows(IllegalArgumentException.class, () -> new Patient("","","","1"));
        assertThrows(IllegalArgumentException.class, () -> new Patient("","","","sdflsdjfewoiw"));
        assertThrows(IllegalArgumentException.class, () -> new Patient("","Name","","12312312345"));
        assertDoesNotThrow((() -> new Patient("Name","Name","","12312312345")));
    }


    @Test
    @DisplayName("Set name throws if blank")
    public void setBlankName() {
        assertThrows(IllegalArgumentException.class, () -> patient.setFirstname(""));
        assertThrows(IllegalArgumentException.class, () -> patient.setLastname(""));
    }

    @Test
    @DisplayName("Changing patient data works")
    public void changingPatientData() {
        patient.setFirstname("Foo");
        assertEquals("Foo",patient.getFirstname());
        patient.setLastname("Foo");
        assertEquals("Foo",patient.getLastname());
        patient.setGeneralPractitioner("Dr. Peders");
        assertEquals("Dr. Peders", patient.getGeneralPractitioner());

        patient.setFirstname("Not foo");
        assertNotEquals("Foo",patient.getFirstname());
        patient.setLastname("Not foo");
        assertNotEquals("Foo",patient.getLastname());
    }

}
