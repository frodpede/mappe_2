package no.frodpede.popups;

import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import no.frodpede.FactoryGUI;
import no.frodpede.Patient;

public class PatientWindow extends Dialog<Patient> {


    public enum Mode {
        ADD, EDIT
    }

    private final Mode mode;

    private Patient existingPatient = null;

    /**
     * Creates a dialog-object and sets mode to add.
     */
    public PatientWindow() {
        super();
        this.mode = Mode.ADD;
        createWindow();
    }

    /**
     * Creates a dialog-object and sets mode to edit.
     *
     * @param patient to be edited.
     */
    public PatientWindow(Patient patient) {
        super();
        mode = Mode.EDIT;
        this.existingPatient = patient;
        createWindow();
    }

    /**
     * Creates a dialog based on the input. If the constructor has a patient, it opens an edit-dialog, if it does not have a patient it opens a add-new-patinet dialog.
     */
    private void createWindow() {
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        switch (mode) {
            case ADD:
                setTitle("Patient - Add");
                break;
            case EDIT:
                setTitle("Patient - Edit");
                break;
        }
        setupWindow();
    }

    /**
     * Sets up the dialog based on the mode fo the dialog.
     */
    private void setupWindow() {
        FactoryGUI factory = new FactoryGUI();
        // Note: unsure if I am supposed to cast to the correct sub-class here, but I don't know how else to do it.

        Label firstNameLabel = (Label) factory.getNode("label");
        firstNameLabel.setText("First name:");
        TextField firstNameInput = (TextField) factory.getNode("textfield");


        Label lastNameLabel = (Label) factory.getNode("label");
        lastNameLabel.setText("Last name:");
        TextField lastNameInput = (TextField) factory.getNode("textfield");

        Label generalPractitionerLabel = (Label) factory.getNode("label");
        generalPractitionerLabel.setText("General Practitioner: ");
        TextField generalPractitionerInput = (TextField) factory.getNode("textfield");

        Label socialSecurityNumberLabel = (Label) factory.getNode("label");
        socialSecurityNumberLabel.setText("Social security number:");
        TextField socialSecurityNumberInput = (TextField) factory.getNode("textfield");


        HBox firstNameBox = (HBox) factory.getNode("hbox");
        firstNameBox.getChildren().setAll(firstNameLabel,firstNameInput);
        firstNameBox.setSpacing(85);

        HBox lastNameBox = (HBox) factory.getNode("hbox");
        lastNameBox.getChildren().setAll(lastNameLabel,lastNameInput);
        lastNameBox.setSpacing(86);

        HBox generalPractitionerBox = (HBox) factory.getNode("hbox");
        generalPractitionerBox.getChildren().setAll(generalPractitionerLabel,generalPractitionerInput);
        generalPractitionerBox.setSpacing(32);

        HBox socialSecurityNumberBox = (HBox) factory.getNode("hbox");
        socialSecurityNumberBox.getChildren().setAll(socialSecurityNumberLabel,socialSecurityNumberInput);
        socialSecurityNumberBox.setSpacing(20);


        VBox root = (VBox) factory.getNode ("vbox");
        root.setSpacing(10);


        if (mode == Mode.ADD) { // if add:
            root.getChildren().setAll(firstNameBox,lastNameBox,socialSecurityNumberBox);
            firstNameInput.setPromptText("First name");
            lastNameInput.setPromptText("Last name");
            generalPractitionerInput.setPromptText("General Practrioner");
            socialSecurityNumberInput.setPromptText("Social security number");

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = null;
                String firstName = firstNameInput.getText().trim();
                String lastName = lastNameInput.getText().trim();
                String generalPractioner = generalPractitionerInput.getText().trim();
                String socialSecurityNumber = socialSecurityNumberInput.getText().trim();

                if (button == ButtonType.OK) {
                    try {
                        patient = new Patient(firstName,lastName, generalPractioner, socialSecurityNumber);
                    } catch (IllegalArgumentException e) {
                        InformationDialog.show("Invalid input", "Patient was not added.\n" + e.getMessage());
                    }
                }
                return patient;
            });

        } else { // if edit:
            root.getChildren().setAll(firstNameBox,lastNameBox);
            firstNameInput.setText(existingPatient.getFirstname());
            lastNameInput.setText(existingPatient.getLastname());
            generalPractitionerInput.setText(existingPatient.getGeneralPractitioner());

            setResultConverter((ButtonType button) -> { // what to return
                Patient patient = existingPatient;
                String firstName = firstNameInput.getText().trim();
                String lastName = lastNameInput.getText().trim();
                String generalPractitioner = generalPractitionerInput.getText().trim();
                if (button == ButtonType.OK) {
                    try {
                        patient.setFirstname(firstName);
                        patient.setLastname(lastName);
                        patient.setGeneralPractitioner("" + generalPractitioner);
                    } catch (IllegalArgumentException e) {
                        InformationDialog.show("Invalid input", "Patient was not edited:\n" + e.getMessage());
                    }
                }
                return patient;
            });
        }

        getDialogPane().setContent(root);

    }
}

