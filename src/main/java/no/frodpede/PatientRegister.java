package no.frodpede;


import java.util.ArrayList;

/**
 * This class register patients.
 * @author Frode
 */
public class PatientRegister {

    private ArrayList<Patient> patients; //list of patients

    public PatientRegister() {
        patients = new ArrayList<>();
    }
    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds a patient to the list if its not already in the list and its not null.
     *
     * @param patient added
     * @throws IllegalArgumentException if a duplicate is found
     */


    public void addPatient(Patient patient) throws IllegalArgumentException {
        if (patients.contains(patient)) {
            throw new IllegalArgumentException("Patient already exists in the register: " + get(patient).toString());
        }
        patients.add(patient);
    }

    /**
     * Removes a patient from the list if the patient exist.
     *
     * @param patient to remove
     * @return true if patient is removed, false if not.
     */

    public boolean removePatient(Patient patient) {
        if (patients.contains(patient)) {
            patients.remove(patient);
            return true;
        }
        return false;

    }

    /**
     * gets a patient from the register
     * @param patient to find
     * @return found patient.
     */

    public Patient get(Patient patient) {
        for (Patient p : patients) {
            if (p.equals(patient)) {
                return p;
            }
        }
        return null;
    }
    public void clear() {
        patients.clear();
    }

}
