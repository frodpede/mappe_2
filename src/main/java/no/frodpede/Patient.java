package no.frodpede;

import java.util.Objects;

/**
 * This class defined the patient
 * @author Frode
 */

public class Patient {

    private String firstname;
    private String lastname;
    private final String socialSecurityNumber;
    private String diagnosis;
    private String generalPractitioner;

    /**
     * Constructor creates an instance of Patient. Social security number ha
     *
     * @param firstname
     * @param lastname
     * @param generalPractitioner
     * @param socialSecurityNumber
     * @throws IllegalArgumentException
     */


    public Patient(String firstname, String lastname, String generalPractitioner, String socialSecurityNumber) throws IllegalArgumentException {
        if (firstname == null || lastname == null || firstname.equals("") || lastname.equals("")) {
            throw new IllegalArgumentException("Name can not be empty. ");
        }
        try {
            Long.parseLong(socialSecurityNumber);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Social security number must be a number.");
        }
        if (socialSecurityNumber.length() != 11) {
            throw new IllegalArgumentException("Social security number must be 11 digits.");
        }
        this.socialSecurityNumber = socialSecurityNumber;
        this.firstname = firstname;
        this.lastname = lastname;
        this.generalPractitioner = generalPractitioner;
    }

    // getters and setters

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }
    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }


    public void setFirstname(String firstname) throws IllegalArgumentException {
        if (firstname == null || firstname.equals("")) {
            throw new IllegalArgumentException("Firstname can not be empty");
        }
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        if (lastname == null || lastname.equals("")) {
            throw new IllegalArgumentException("Lastname can not be empty");
        }
        this.lastname = lastname;
    }

    /**
     * Checks if a object is equal to the classes objeect.
     *
     * @param o object that checked
     * @return true if equal, false if not
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(socialSecurityNumber, patient.socialSecurityNumber);
    }

    /**
     * Equals method based on ssn
     * @return hash code for the object
     */

    @Override
    public int hashCode() {
        return Objects.hash(socialSecurityNumber);
    }

    /**
     * to string method
     * @return firstname, lastname and ssn
     */
    @Override
    public String toString() {
        return firstname + " " + lastname + " (" +socialSecurityNumber + ")";
    }
}
