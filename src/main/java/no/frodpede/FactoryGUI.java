package no.frodpede;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * Implementing factory-design to create JavaFX-nodes.
 */
public class FactoryGUI {

    public Node getNode(String type){
        if (type.equalsIgnoreCase("Button")){
            return new Button();
        }
        else if (type.equalsIgnoreCase("TextField")){
            return new TextField();
        }
        else if (type.equalsIgnoreCase("VBOX")) {
            return new VBox();
        }
        else if (type.equalsIgnoreCase("HBOX")) {
            return new HBox();
        }
        else if (type.equalsIgnoreCase("Label")){
            return new Label();
        }
        else if (type.equalsIgnoreCase("Gridpane")){
            return new GridPane();
        }
        else if (type.equalsIgnoreCase("TableView")){
            return new TableView<>();
        }
        else if(type.equalsIgnoreCase("ImageView")){
            return new ImageView();
        }
        else if (type.equalsIgnoreCase("MenuBar")){
            return new MenuBar();
        }
            else if (type.equalsIgnoreCase("TOOLBAR")) {
            return new ToolBar();
        }
        return null;
    }

}
