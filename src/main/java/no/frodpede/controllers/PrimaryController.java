package no.frodpede.controllers;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import no.frodpede.*;
import no.frodpede.popups.InformationDialog;
import no.frodpede.popups.PatientWindow;
import no.frodpede.saving.FileHandler;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Primery controller for the application.
 * @author Frode
 */


public class PrimaryController {


    @FXML
    private Label statusLabel;
    @FXML
    private Button removePasientButton;
    @FXML
    private Button editPasientButton;
    @FXML
    private Button addPatientButton;
    @FXML
    private TableView<Patient> patientTableView;
    @FXML
    private TableColumn<Patient, String> firstnameColumn;
    @FXML
    private TableColumn<Patient, String> lastnameColumn;
    @FXML
    private TableColumn<Patient, String> generalPractitionerColumn;
    @FXML
    private TableColumn<Patient, String> socialSecurityNumberColumn;


    ObservableList<Patient> patientsObservable = FXCollections.observableArrayList();

    PatientRegister patientRegister = new PatientRegister();

    private Patient selectedPatient;


    /**
     * Initializes the controller.
     */

    public void initialize() {

        firstnameColumn.setCellValueFactory(new PropertyValueFactory<>("firstname"));
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<>("lastname"));
        generalPractitionerColumn.setCellValueFactory(new PropertyValueFactory<>("generalPractitioner"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));

        patientTableView.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getButton().equals(MouseButton.PRIMARY) || mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                selectedPatient = patientTableView.getSelectionModel().getSelectedItem();
                System.out.println("Selected item: " + selectedPatient);
            }
        });

        patientTableView.setItems(patientsObservable);

        statusLabel.setText("OK");

    }
    private void updateObservableList() {
        this.patientsObservable.setAll(patientRegister.getPatients());
    }

    /**
     * Opens patient window and adds a patient from the users input.
     */

    @FXML
    public void addNewPatient() {
        //Since there is no argument passed in the constructor to create a patient is used
        PatientWindow patientWindow = new PatientWindow();
        Optional<Patient> result = patientWindow.showAndWait();

        if (result.isPresent()) {
            Patient newPatient = result.get();

            try {
                patientRegister.addPatient(newPatient);
                updateObservableList();
                System.out.println("Patient \"" + newPatient + "\" added!");
            } catch (IllegalArgumentException e) {
                InformationDialog.show("Patient not added",e.getMessage());
                System.out.println("Patient not added: " + e.getMessage());
            }

        } else {
            System.out.println("Patient adding cancelled.");
        }
    }

    /**
     * Edits a selected patient and opens a edit window.
     */

    @FXML
    private void editPatient() {
        if (selectedPatient != null) {
            PatientWindow dialog = new PatientWindow(selectedPatient);

            Optional<Patient> result = dialog.showAndWait();

            if (result.isPresent()) {
                selectedPatient = result.get();
                updateObservableList();
            } else {
                System.out.println("Patient editing cancelled.");
            }
        }
    }

    /**
     * Removes a selected patient if the user answers "OK" in the confirmation window.
     */
    @FXML
    private void removePatient() {

        if(selectedPatient != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Confirmation");
            alert.setHeaderText("Delete Confirmation");
            alert.setContentText("Are you sure you want to delete patient \n\"" + selectedPatient + "\" from the " +
                    "register?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                patientRegister.removePatient(selectedPatient);
                updateObservableList();
                System.out.println("Removed patient \"" + selectedPatient + "\" from the register");
                selectedPatient = null;
            } else {
                System.out.println("Patient deletion cancelled.");
            }
        }
    }

    /**
     * Allow the user to import .csv files.
     * Opens the filepath window where you can import the files from.
     */

    @FXML
    public void readFromFile() {
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = getFileChooser();
        File file = fileChooser.showOpenDialog(new Stage());
        ArrayList<Patient> duplicates = new ArrayList<>(); // if there are any duplicates, they are added here

        if (file != null) {
            ArrayList<Patient> readPatients;
            try {
                patientRegister.clear();
                readPatients = fileHandler.readCSVFile(file.getAbsolutePath());
                for (Patient p : readPatients) {
                    try {
                        patientRegister.addPatient(p);
                    } catch (IllegalArgumentException e) {
                        System.out.println("Patient \"" + p + "\" not added: " + e.getMessage());
                        duplicates.add(p);
                    }
                }
                updateObservableList();
                statusLabel.setText("Import successful!");
                System.out.println("Import successful!");
                /* In future, get all the duplicates and ask the user which ones they want to overwrite with, but for
                now an alert is sufficient: */
                if (!duplicates.isEmpty()) {
                    InformationDialog.show("Duplicate patient found",duplicates.size() + " patient(s) were not added" +
                            " because a patient with the same social security numbers already exist in the register.");
                }
            } catch (IOException e) {
                statusLabel.setText("Error: file was not imported.");
                e.printStackTrace();
            }
        } else {
            System.out.println("Import from file cancelled.");
        }
    }

    /**
     * Allows you to export the information in the application to a .csv-file.
     * Opens a the filepath where the user can save the file.
     */

    @FXML
    private void exportToFile() {
        FileHandler fileHandler = new FileHandler();
        FileChooser fileChooser = getFileChooser();
        File file = fileChooser.showSaveDialog(new Stage());

        if(file != null) {
            try {
                fileHandler.writeCSVFile(patientRegister.getPatients(), file.getAbsolutePath());
                statusLabel.setText("Export successful!");
                System.out.println("Export successful!");
            } catch (IOException e) {
                statusLabel.setText("Error: file was not exported.");
                e.printStackTrace();
            }
        } else {
            System.out.println("Export to file cancelled");
        }
    }


    private void clearPatientRegister(){
        patientRegister.getPatients().clear();
    }


    private Patient selectedPatient(){
        return patientTableView.getSelectionModel().getSelectedItem();
    }

    /**
     * Gets a FileChooser
     *
     * @return a FileChooser
     */

    private FileChooser getFileChooser() {
        // default directory (resource folder)
        String path = FileSystems.getDefault().getPath("").toAbsolutePath() +
                "\\src\\main\\resources\\no\\frodpede\\PatientFiles";
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File(path));
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter(".csv", "*.csv"));

        return fc;
    }

    /**
     * Exits the application.
     */
    @FXML
    public void exitApplication(){
        Platform.exit();
    }

}