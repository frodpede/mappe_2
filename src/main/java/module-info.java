module no.frodpede {
    requires javafx.controls;
    requires javafx.fxml;

    opens no.frodpede to javafx.fxml;
    exports no.frodpede;
    exports no.frodpede.controllers;
    opens no.frodpede.controllers to javafx.fxml;
    exports no.frodpede.popups;
    opens no.frodpede.popups to javafx.fxml;
}